from django.conf.urls import url
from shortener import views


urlpatterns = [
    url(r'^$', views.ShortnerIndexPageView.as_view(), name="shortner-index"),
    url(r'^inflator/$', views.InflatorPageView.as_view(), name="inflator-url"),
    url(r'^(?P<short_code>[\w-]+)/$', views.UrlRedirectView.as_view(), name='url-redirect'),
    url(r'^404/$', views.PageNotFoundView.as_view(), name="404-page"),
    url(r'^500/$', views.ServerErrorView.as_view(), name="500-page"),
    url(r'^400/$', views.BadRequestView.as_view(), name="400-page"),
]
