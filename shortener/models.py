import django.db.models
from django.conf import settings
import re

import shortener.utils


class ShortUrl(django.db.models.Model):
    full_url = django.db.models.TextField(null=False, blank=False)
    short_url = django.db.models.CharField(
        null=False, blank=False, unique=True,
        max_length=settings.SHORT_CODE_MAX
    )
    date_modified = django.db.models.DateTimeField(null=True, blank=True, auto_now=True)
    date_created = django.db.models.DateTimeField(null=True, blank=True, auto_now_add=True)
    is_active = django.db.models.BooleanField(blank=True, default=True)

    def save(self, *args, **kwargs):
        """
        Over ride the save.
        :param args: 
        :param kwargs: 
        :return: 
        """
        if self.short_url is None or self.short_url == "":
            self.short_url = ShortUrl.create_short_code(self)
        if "http" not in self.full_url:
            self.full_url = "http://" + self.full_url
        super(ShortUrl, self).save(*args, **kwargs)

    def create_short_code(self, no_chars=settings.SHORT_CODE_MIN):
        """
        Create an acceptable short code which we can use and which is unique.
        :param no_chars: Length of short code (int)
        :return: The short code (str)
        """
        new_code = shortener.utils.ShortenerUtil.short_code_generator(no_chars=no_chars)
        qs_exists = ShortUrl.objects.filter(short_url=new_code).exists()
        if qs_exists:
            return self.create_short_code()
        return new_code

    def get_short_url(self):
        short_url = str(settings.ROOT_URL) + '/' + str(self.short_url)
        return str(short_url)

    @staticmethod
    def get_create_short_code(str_full_url):
        """
        Get or create the short code for a provided full url.
        This checks for if the full url provided is already a shorten url of an url - if so then it will not
            allow to create another record for the same url and will get the required data (the matching full url data
            instead of the short url data for the full url provided) and will say already a shorten url in return.
        This check if the provided full url exist then it will not create instead it will get the required 
            data (short url details) for the provided full url.
        This create a new only if not short code exit for the provided full url and after creating so it will get the 
            required data (short url details) for the provided full url.
        :param str_full_url: a url (str)
        :return: a data dictionary (dict)
        """
        # check if already a shorten url - this is when an already shorten url provided as the full url
        str_short_code = re.split('\\b' + settings.ROOT_DOMAIN + '\\b', str_full_url)[-1]

        if str_short_code.startswith('/'):
            str_short_code = str_short_code[1:]
            qs = ShortUrl.objects.filter(short_url__iexact=str_short_code)
            if qs.count() > 0:
                # yes already a shorten url -  don't allow to create a shorten url of a already shorten url.
                data_dictionary = {
                    'ret_message': 'Already a Shorten Url', 'is_success': False,
                    'url_dict': {'short_url': str(qs.first().full_url)}
                }
                return data_dictionary

        # not already a shorten url so either get the data (if matching record for full url exist) else create one
        the_url, created = ShortUrl.objects.get_or_create(full_url=str_full_url)
        data_dictionary = {
            'ret_message': '', 'is_success': True,
            'url_dict': {'short_url': str(the_url.get_short_url())}
        }

        return data_dictionary

    @staticmethod
    def get_full_url_data(str_short_url):
        """
        Get Full URL Data for a provided short code/short url (if matching record exist).
        :param str_short_url: any string (str)
        :return: a data dictionary (dict)
        """
        str_short_code = re.split('\\b' + settings.ROOT_DOMAIN + '\\b', str_short_url)[-1]
        str_short_code = re.sub(r'/', '', str_short_code)
        qs = ShortUrl.objects.filter(short_url__iexact=str_short_code)

        if qs.count() > 0:
            # exist/match
            the_url = qs.first()
            data_dictionary = {
                'ret_message': '', 'is_success': True,
                'url_dict': {'full_url': str(the_url.full_url)}
            }
        else:
            # no match
            ret_message = 'No Full URL found for the provided Input!'
            data_dictionary = {
                'ret_message': ret_message, 'is_success': False,
                'url_dict': {'full_url': ''}
            }
        return data_dictionary
