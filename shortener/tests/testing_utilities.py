import time


class TestUtil:

    def __init__(self):
        pass

    @staticmethod
    def print_test_time_elapsed(method):
        """
        Utility method for print verbalizing test suite, prints out
        time taken for test and functions name, and status
        """
        def run(*args, **kw):
            if 'SETUP' not in str(method.__name__).upper():
                print("\n======================================================================")
                ts = time.time()
                print('\ttesting function %r' % method.__name__)
                method(*args, **kw)
                te = time.time()
                print('\t[OK] \t in %r \t %2.2f sec \t ' % (method.__name__, te - ts))
                print("\n")

        return run

    @staticmethod
    def append_csrf_token(the_client, url, data):
        resp = the_client.get(url)
        data['csrfmiddlewaretoken'] = resp.cookies['csrftoken'].value
        return data
