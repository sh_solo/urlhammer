import factory
import shortener.models


class ShortUrlFactory(factory.Factory):

    class Meta:
        model = shortener.models.ShortUrl

    full_url = 'www.google.com'
    short_url = 'j0nv1'
