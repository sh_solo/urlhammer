from django.test import TestCase
from django.test import Client

from django.core.urlresolvers import reverse
import shortener.tests.testing_utilities
import shortener.tests.model_factory
import shortener.forms


class ViewTests(TestCase):

    @shortener.tests.testing_utilities.TestUtil.print_test_time_elapsed
    def setUp(self):
        self.client = Client(enforce_csrf_checks=True)

    @shortener.tests.testing_utilities.TestUtil.print_test_time_elapsed
    def test_shortener_get_view(self):
        response = self.client.get(reverse('shortner-index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'URL SHORTENER')

    @shortener.tests.testing_utilities.TestUtil.print_test_time_elapsed
    def test_shortener_post_view(self):
        data = {'ret_message': 'testing post with unittest passing csrf token too'}
        data = shortener.tests.testing_utilities.TestUtil\
            .append_csrf_token(self.client, reverse('shortner-index'), data)
        response = self.client.post(reverse('shortner-index'), data=data)
        self.assertEqual(response.status_code, 200)

    @shortener.tests.testing_utilities.TestUtil.print_test_time_elapsed
    def test_valid_shortener_form(self):
        the_url = shortener.tests.model_factory.ShortUrlFactory.create(
            full_url='www.urlhammer.com',
        )
        data = {'full_url': the_url.full_url}
        form = shortener.forms.UrlShortenerForm(data=data)
        self.assertTrue(form.is_valid())

    @shortener.tests.testing_utilities.TestUtil.print_test_time_elapsed
    def test_invalid_shortener_form(self):
        the_url = shortener.tests.model_factory.ShortUrlFactory.create(
            full_url='Some Not Url Str',
        )
        data = {'full_url': the_url.full_url}
        form = shortener.forms.UrlShortenerForm(data=data)
        self.assertFalse(form.is_valid())
