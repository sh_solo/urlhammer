from django.test import LiveServerTestCase

from django.core.urlresolvers import reverse
from selenium import webdriver
import shortener.tests.testing_utilities
import shortener.tests.model_factory
import shortener.forms


class TestShortenerForm(LiveServerTestCase):

    def setUp(self):
        self.selenium = webdriver.Firefox()

    def tearDown(self):
        self.selenium.quit()

    def _get_full_url(self, namespace):
        """
        Auxiliary function to add view subdir to URL
        :param namespace: 
        :return: 
        """
        return self.live_server_url + reverse(namespace)

    @shortener.tests.testing_utilities.TestUtil.print_test_time_elapsed
    def test_fire_shortener_form(self):
        driver = self.selenium
        driver.get(self._get_full_url('shortner-index'))
        elem = driver.find_element_by_name('full_url')
        #elem.clear()
        #elem.send_keys('www.urlhammer.com')
        driver.find_element_by_id('sh_id').click()
        self.assertIn(self._get_full_url('shortner-index'), self.selenium.current_url)
