from django.test import TestCase

import shortener.models
import shortener.tests.model_factory
import shortener.tests.testing_utilities


class TestShortUrl(TestCase):

    def setUp(self):
        self.url_one = shortener.tests.model_factory.ShortUrlFactory.create(
            full_url='www.urlhammer.com',
            short_url='x3hyd',
        )

    def tearDown(self):
        pass

    @shortener.tests.testing_utilities.TestUtil.print_test_time_elapsed
    def test_url_one_created(self):
        self.assertEqual(self.url_one.full_url, 'www.urlhammer.com')
        self.assertEqual(self.url_one.short_url, 'x3hyd')

    @shortener.tests.testing_utilities.TestUtil.print_test_time_elapsed
    def test_url_two_created(self):
        url_two = shortener.tests.model_factory.ShortUrlFactory.create(
            full_url='www.google.com',
            short_url='j0nv1',
        )

        self.assertEqual(url_two.full_url, 'www.google.com')
        self.assertNotEqual(self.url_one.short_url, url_two.short_url)
