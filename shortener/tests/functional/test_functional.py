from django.test import LiveServerTestCase

from django.core.urlresolvers import reverse
from selenium import webdriver
import shortener.tests.testing_utilities


class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        self.selenium = webdriver.Firefox()

    def tearDown(self):
        self.selenium.quit()

    def _get_full_url(self, namespace):
        """
        Auxiliary function to add view subdir to URL
        :param namespace: 
        :return: 
        """
        return self.live_server_url + reverse(namespace)

    @shortener.tests.testing_utilities.TestUtil.print_test_time_elapsed
    def test_page_src(self):
        """
        Tests that page is loading properly
        """
        self.selenium.get(self._get_full_url('shortner-index'))
        self.assertIn(u'URL SHORTENER', self.selenium.page_source)  # this works
