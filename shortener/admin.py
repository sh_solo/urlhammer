from django.contrib import admin

# Register your models here.
import shortener.models


class ShortenerAdmin(admin.ModelAdmin):
    model = shortener.models.ShortUrl
    list_display = [
        'full_url', 'short_url', 'is_active', 'date_created',
    ]
    search_fields = ['id', 'full_url', 'short_url', ]
    list_filter = ['is_active']

    def has_add_permission(self, request):
        # disallow to add
        return False

    def has_delete_permission(self, request, obj=None):
        # hide the delete button
        return False

admin.site.register(shortener.models.ShortUrl, ShortenerAdmin)
