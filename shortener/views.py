from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import Http404
from django.template import loader
from django.views.generic import View
from django.contrib import messages

import shortener.models
import shortener.forms


class ShortnerIndexPageView(View):

    @staticmethod
    def get(request):
        shortener_form = shortener.forms.UrlShortenerForm()
        data_dictionary = {'form': shortener_form}
        template_loader = loader.get_template('shortner_index.html')
        return HttpResponse(template_loader.render(data_dictionary, request))

    @staticmethod
    def post(request):
        is_success = False
        template_loader = loader.get_template('shortner_index.html')
        shortener_form = shortener.forms.UrlShortenerForm(request.POST)

        if shortener_form.is_valid():
            str_full_url = shortener_form.cleaned_data.get('full_url', '')
            data_dictionary = shortener.models.ShortUrl.get_create_short_code(str_full_url)
            data_dictionary['form'] = shortener_form
            return HttpResponse(template_loader.render(data_dictionary, request))
        else:
            messages.error(request, 'Please enter a valid url!')
            data_dictionary = {
                'form': shortener_form, 'ret_message': '', 'is_success': is_success,
                'url_dict': {'short_url': ''}
            }
            return HttpResponse(template_loader.render(data_dictionary, request))


class InflatorPageView(View):

    @staticmethod
    def get(request):
        inflator_form = shortener.forms.UrlInflatorForm()
        data_dictionary = {'form': inflator_form}
        template_loader = loader.get_template('inflator_index.html')
        return HttpResponse(template_loader.render(data_dictionary, request))

    @staticmethod
    def post(request):
        is_success = False
        template_loader = loader.get_template('inflator_index.html')
        inflator_form = shortener.forms.UrlInflatorForm(request.POST)

        if inflator_form.is_valid():
            str_short_url = inflator_form.cleaned_data.get('short_url', '')
            data_dictionary = shortener.models.ShortUrl.get_full_url_data(str_short_url)
            data_dictionary['form'] = inflator_form
            return HttpResponse(template_loader.render(data_dictionary, request))
        else:
            # invalid string (from form validation)
            data_dictionary = {
                'form': inflator_form, 'ret_message': '', 'is_success': is_success,
                'url_dict': {'full_url': ''}
            }
            return HttpResponse(template_loader.render(data_dictionary, request))


class UrlRedirectView(View):

    @staticmethod
    def get(request, short_code=None):
        qs = shortener.models.ShortUrl.objects.filter(short_url__iexact=short_code)

        if qs.count() != 1 and not qs.exists():
            raise Http404
        the_url = qs.first()
        return HttpResponseRedirect(the_url.full_url)


class PageNotFoundView(View):

    @staticmethod
    def get(request):
        template_loader = loader.get_template('shortner_error_wrap.html')
        data_dictionary = {
            'message_title': '404',
            'message_body': 'Page not found, this is not the page that you are looking for.',
            'message_sub_body': '',
        }
        return HttpResponse(template_loader.render(data_dictionary, request))


class ServerErrorView(View):

    @staticmethod
    def get(request):
        template_loader = loader.get_template('shortner_error_wrap.html')
        data_dictionary = {
            'message_title': '500',
            'message_body': 'Internal Server Error, it seems there is a problem with our server.',
            'message_sub_body': 'Please try again in a moment or send us a mail regarding this issue.',
        }
        return HttpResponse(template_loader.render(data_dictionary, request))


class BadRequestView(View):

    @staticmethod
    def get(request):
        template_loader = loader.get_template('shortner_error_wrap.html')
        data_dictionary = {
            'message_title': '400',
            'message_body': 'Not a good request -  Bad Request.',
            'message_sub_body': '',
        }
        return HttpResponse(template_loader.render(data_dictionary, request))
