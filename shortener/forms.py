from django import forms
from django.conf import settings
import re


class UrlShortenerForm(forms.Form):
    full_url = forms.URLField(
        label='',
        required=True,
        widget=forms.TextInput(
            attrs={
                "placeholder": "URL (ex: www.urlhammer.com)",
                "class": "form-control input-lg"
                }
        )
    )


class UrlInflatorForm(forms.Form):
    short_url = forms.CharField(
        label='',
        required=True,
        widget=forms.TextInput(
            attrs={
                "placeholder": "Shorten URL (ex: www.urlhammer.com/qfgr1)",
                "class": "form-control input-lg"
                }
        )
    )

    def clean(self):
        cleaned_data = super(UrlInflatorForm, self).clean()

        str_short_url = cleaned_data.get('short_url', '')
        str_short_code = re.split('\\b' + settings.ROOT_DOMAIN + '\\b', str_short_url)[-1]
        ct = str_short_code.count('/')

        if any(x in str_short_code for x in ['www', 'http', 'https']):
            # if its a url but has no short code at the end
            if ct == 0:
                raise forms.ValidationError({'short_url': ["Not a Valid Shorten Input!", ]})
        if settings.ROOT_DOMAIN in str_short_url:
            # if its our domain url
            if ct > 2:
                raise forms.ValidationError({'short_url': ["Not a Valid Shorten Input!", ]})
        else:
            if ct > 0:
                # not our domain and no short code in string at all
                raise forms.ValidationError({'short_url': ["Not a Valid Shorten Input!", ]})
