from django.conf import settings
import string
import random


class ShortenerUtil(object):

    @staticmethod
    def short_code_generator(no_chars=settings.SHORT_CODE_MIN, chars=string.ascii_lowercase + string.digits):
        """
        Generate short code.
        :param no_chars: Length of short code (int)
        :param chars: Allowed characters for short code (str)
        :return: The short code (str)
        """
        return ''.join(random.choice(chars) for _ in range(no_chars))
