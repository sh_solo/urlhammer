from urlhammer.settings.base import *

DEBUG = False

# if debug turns to False then set the host
ALLOWED_HOSTS = [
    '127.0.0.1',
    'urlhammer.com',
    'www.urlhammer.com',
    'urlhammer.ivf5kmei6q.ap-southeast-1.elasticbeanstalk.com',
    'www.urlhammer.ivf5kmei6q.ap-southeast-1.elasticbeanstalk.com'
]

ROOT_DOMAIN = 'www.urlhammer.com'
ROOT_URL = 'http://' + ROOT_DOMAIN
