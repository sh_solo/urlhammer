from urlhammer.settings.base import *

DEBUG = True

# if debug turns to False then set the host
ALLOWED_HOSTS = ['127.0.0.1']
