#
# OVERVIEW  #
#

	URL Shortening service.

	It will take long URL as input and shorten it and it
	can inflate a shortened URL to its original form. 

	When enter the shorten url in a browser then it will
	redirect the age to the original website.  

	1. Shorten URL 
		If we enter a long url something like
		"https://www.google.com/search?q=new+in+py"
		 then it will give something like
		"http://www.urlhammer.com/b2kz5"
		- i.e. the shorten url of a full url.  

	2. Inflate URL (form shorten url to original form) 
		If we enter a Shorten URL or the Shorten Code
		then it will return back the original URL. 
		For example if we enter
		"http://www.urlhammer.com/b2kz5" or "b2kz5" then
		it will return something like
		"https://www.google.com/search?q=new+in+py"
		- i.e. the original url form the shorten url.

	3. Redirect Shorten URL to the original/right site 
		For example if we enter
		"http://www.urlhammer.com/b2kz5" in browser then
		it will redirect to something like
		"https://www.google.com/search?q=new+in+py"
		- i.e. to the original site.

	4. It has admin for URL Shorten data
		(to see it have to login as admin)



#
#
#
# DEMO #
Please Visit http://urlhammer.com
#
#
#



#
#
#
# Common Prerequisites #
#
	Python >= 3.4
	Django >= 1.10
	MySQL >= 5.0

	Note: As this project hosted on AWS therefore
		awsebcli package will be needed for deployment
		to live server purpose (see under
		"DEPLOY to Amazon Web Services" section bellow).
		AWS related packages(i.e awsebcli) will not be
		needed if want to run only in localhost.



#
#
#
# INSTALLATION #
#

	1. Let say we want to set up the project
		under "py_project" folder Contains
		so move to that folder i.e.
			cd /path/to/py_project/

	2. Create Virtual Environment for the project. 
		Create the virtual environment any way you like. 
		(I created virtual environment using
		python3 -m venv --without-pip v_env  )

	3. Activate the virtual environment
			source v_env/bin/activate

	4. Download the project and unzip it.
		(ex the unzipped folder name is "urlhammer-master").
		Copy the unzipped folder "urlhammer-master"
		and paste it under "py_project:

	5. Now move to "urlhammer-master" folder i.e.
			cd /path/to/py_project/urlhammer-master/  

	6. Time to install the dependencies
		Run the following
		(make sure virtual environment is active)
			pip3 install -r requirements.txt  

	7. Apply migrations
		Allpy migrations after createing a database.
		(The ddatabase neame I am using urlhammer,
		create a dabase of any name you prefere.
		Change the database credentials in settings
		file - in this case it is the base setting
		file under settings folder.)

	8. Now can runs the server and see the app in browser.
			python3 manage.py runserver  

	NOTE: To change settings variables
		you can find the setting file at
			/path/to/py_project/urlhammer-master/settings/



#
#
#
# DEPLOY to Amazon Web Services (AWS) #
#

	1. Install AWS CLI support  (Amazon Elastic Beanstalk)
			pip3 install awsebcli
			(not needed to run as it as it is
			already done when we ran
			pip3 install -r requirements.txt)  

	2. Initialize the environment  
			eb init -p python3.4 urlhammer  
			(as the aws environment is python 3.4)

	3. Create the environment
			eb create urlhammer

	4. Specify which environment to use
			eb use url hammer

	5. Deploy the project to AWS
			eb deploy urlhammer (or simply run eb deploy)



#
#
#
# TEST #
#

	The the cases (both unit test and functional test)
	will be found under the tests/ directory
	(under shortener app in this case) .

	These tests can be run using either coverage or
	using python3 . i.e. test can be run
	either like  
		coverage run manage.py test shortener.tests.unit.test_models.TestShortUrl.test_url_one_created
	or like
		pypython3 manage.py test shortener.tests.unit.test_models.TestShortUrl.test_url_one_created

	SOME TEST CASES:

		A. UNIT TEST

			1. Model - Test of short code creation 
				Test of short code creation with passing
					correct data - short url as  
						full_url='www.urlhammer.com',
		            	short_url='x3hyd',

				coverage run manage.py test shortener.tests.unit.test_models.TestShortUrl.test_url_one_created 
				(or coverage run manage.py test shortener.tests.unit.test_models to run all test for model ) 

			2. View - Test Shortener View - GET method Test

				coverage run manage.py test shortener.tests.unit.test_views.ViewTests.test_shortener_get_view 

			3. View - Test Shortener - Post Method Test With CSRF Token 

				coverage run manage.py test shortener.tests.unit.test_views.ViewTests.test_shortener_post_view 

			4. Form - Test Shortener Valid Form
				Passing correct format of full_url as 'www.urlhammer.com'

				coverage run manage.py test shortener.tests.unit.test_views.ViewTests.test_valid_shortener_form 

			5. Form - Test Shortener Invalid Form 
				Passing incorrect format of full_url
				as 'Some Not Url Str'.

				coverage run manage.py test shortener.tests.unit.test_views.ViewTests.test_invalid_shortener_form 

			6. Form - Fire the form 

				coverage run manage.py test shortener.tests.unit.test_forms.TestShortenerForm.test_fire_shortener_form

		B. FUNCTIONAL TEST

			Test that page is loading properly

			coverage run manage.py test shortener.tests.functional
#
#
#
